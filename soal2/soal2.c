#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>

#define HOME "/home/yasha"
#define ZIPFILE HOME "/drakor.zip"


void create_dir(char *dirname)
{
    pid_t pid = fork();
    int status;
    if (pid == 0)
    {
        char *argv[] = {"mkdir", dirname, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(0);
    }
    wait(&status);
}


void extract(char *zippath, char *extractpath)
{
    pid_t pid = fork();
    int status;
    if (pid == 0)
    {
        char *argv[] = {"unzip", "-q", zippath, "-d", extractpath, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    wait(&status);
}

void hapus_tidak_penting()
{
    struct dirent *dp;
    DIR *dir = opendir(HOME "/shift2/drakor");

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        char *d_name = malloc(strlen(dp->d_name) + 1);
        strcpy(d_name, dp->d_name);
        if (strcmp(d_name + strlen(d_name) - 4, ".png") != 0 && strcmp(d_name, ".") != 0 && strcmp(d_name, "..") != 0)
        {
            pid_t pid = fork();
            int status;
            char *to_delete = malloc(100);
            strcpy(to_delete, HOME "/shift2/drakor/");
            strcat(to_delete, d_name);
            if (pid == 0) 
            {
                char *argv[] = {"rm", to_delete, "-rf", NULL};
                execv("/usr/bin/rm", argv);
                exit(0);
            }
            wait(&status);
        }
    }
    closedir(dir);
}

void main()
{
    create_dir(HOME "/shift2");
    create_dir(HOME "/shift2/drakor");
    extract(ZIPFILE, HOME "/shift2/drakor");
    hapus_tidak_penting();
}