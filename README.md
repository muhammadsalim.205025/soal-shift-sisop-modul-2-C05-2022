# Soal Shift Sisop Modul 2 C05 2022

## Anggota Kelompok ##

NRP | NAMA
------------- | -------------
5025201057    | Muhammad Fuad Salim
5025201112    | Naufal Ariq Putra Yosyam
5025201139    | Nabila Zakiyah Khansa' Machrus

# SOAL 1
A. Mendownload file "characters.zip" dan "weapons.zip" dengan menggunakan link yang telah ada, kemudian kedua file tersebut akan di extract. Setelah itu akan dibuat 1 direktori bernama "gacha_gacha" dan seluruh file characters dan weapons akan dipindahkan ke folder tersebut.

```c
void download_and_extract(){
    char *links[] = {"https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
                        "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"};
    
    char *file[] = {
                     "characters.zip","weapons.zip" 
                    };

        int status;
        pid_t child = fork();

    if(child < 0){
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < 2 ; i ++){
        if(child == 0){
            char *argv[] = {"wget", "-q", "--no-check-certificate", links[i], "-O", file[i], NULL};
            execv("/bin/wget", argv);
        }
        while((wait(&status)) > 0);
        child = fork();
    }

    for (int i = 0; i < 2 ; i ++){
        if(child == 0){
            char *argv[] = {"unzip", file[i], NULL};
            execv("/bin/unzip", argv);
        }
        while((wait(&status)) > 0);
        child = fork();
    }
}
```

Fungsi download_and_extract() digunakan untuk mendownload file sekaligus mengekstrak file tersebut. Pada fungsi ini menggunakan bantuan execv dengan syntax wget dan juga unzip.

```c
void make_directory(){
    int status;

    if(fork() == 0){
        char *argv[] = {"mkdir", "/home/mfuadsalim/SISOP/MODUL2/soal1/gacha_gacha",NULL};
        execv("/bin/mkdir", argv);
    }

}
```

Fungsi make_directory() digunakan untuk membuat direktori atau folder baru yang bernama "gacha_gacha". Pada fungsi ini menggunakan bantuan execv dengan syntax mkdir.

```c
void move_then_delete(){
    pid_t child = fork();
    int status;

    char cwd[PATH_MAX], path[PATH_MAX];
    getcwd(cwd, PATH_MAX);

    DIR *dir;
    struct dirent *dp;
    char current[PATH_MAX];
    char dest[PATH_MAX];

    char *file[] = {"characters", "weapons"};

    for (int i = 0; i < 2; i++){
        strcpy(current, cwd);
        strcpy(dest, "/home/mfuadsalim/SISOP/MODUL2/soal1/gacha_gacha/"); 
        dir = opendir(current);

        while((dp = readdir(dir)) != NULL){
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 strstr(dp->d_name, "soal") == 0){
                if(child == 0){
                    char file[PATH_MAX];
                    strcpy(file, current);
                    strcat(file, "/");
                    strcat(file, dp->d_name);
                    
                    char *argv[] = {"mv", file, dest, NULL};
                    execv("/bin/mv", argv);
                }
                while((wait(&status)) > 0);
                child = fork();
            }
        }
        closedir(dir);
    }

    strcpy(path, "/home/mfuadsalim/SISOP/MODUL2/soal1/gacha_gacha/");
    dir = opendir(path);

    while((dp = readdir(dir)) != NULL){
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strcmp(dp->d_name, "weapons") != 0 && strcmp(dp->d_name, "characters") != 0){

            strcpy(path, "/home/mfuadsalim/SISOP/MODUL2/soal1/gacha_gacha/");
            strcat(path, dp->d_name);

            if(child == 0){
                char *argv[] = {"rm", "-rf", path, NULL};
                execv("/bin/rm", argv);
            }
            while((wait(&status)) > 0);
            child = fork();
        }
    }
    closedir(dir);
}
```

Fungsi move_then_delete() digunakan untuk memindahkan file-file yang telah di ekstrak kemudian akan dipindahkan ke destinasini, yaitu folder "gacha_gacha". Pada fungsi ini menggunakan bantuan execv dengan syntax mv dan rm -rf.


# SOAL 2
A. Mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Program harus bisa membedakan file dan folder sehingga menghapus folder-folder yang tidak dibutuhkan.
```c
void create_dir(char *dirname)
{
    pid_t pid = fork();
    int status;
    if (pid == 0)
    {
        char *argv[] = {"mkdir", dirname, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(0);
    }
    wait(&status);
}


void extract(char *zippath, char *extractpath)
{
    pid_t pid = fork();
    int status;
    if (pid == 0)
    {
        char *argv[] = {"unzip", "-q", zippath, "-d", extractpath, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    wait(&status);
}

void hapus_tidak_penting()
{
    struct dirent *dp;
    DIR *dir = opendir(HOME "/shift2/drakor");

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        char *d_name = malloc(strlen(dp->d_name) + 1);
        strcpy(d_name, dp->d_name);
        if (strcmp(d_name + strlen(d_name) - 4, ".png") != 0 && strcmp(d_name, ".") != 0 && strcmp(d_name, "..") != 0)
        {
            pid_t pid = fork();
            int status;
            char *to_delete = malloc(100);
            strcpy(to_delete, HOME "/shift2/drakor/");
            strcat(to_delete, d_name);
            if (pid == 0) 
            {
                char *argv[] = {"rm", to_delete, "-rf", NULL};
                execv("/usr/bin/rm", argv);
                exit(0);
            }
            wait(&status);
        }
    }
    closedir(dir);
}

void main()
{
    create_dir(HOME "/shift2");
    create_dir(HOME "/shift2/drakor");
    extract(ZIPFILE, HOME "/shift2/drakor");
    hapus_tidak_penting();
}
```
 
## REVISI ## 

B. Mengategorikan poster drama sesuai jenis, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip.
```c
void create_category_dirs()
{
    struct Node *tmp = head;
    do
    {
        char *name = malloc(100);
        strcpy(name, HOME);
        strcat(name, "/shift2/drakor/");
        strcat(name, tmp->drakor->kategori);
        DIR *dir = opendir(name);
       
        if (dir)
        {
            closedir(dir);
        }
        else
        {
            create_dir(name);
        }
    } while (tmp = tmp->next);
}
```

C. Memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.

D. Jika ada  satu foto yang memiliki  lebih dari satu poster maka dipindahkan sesuai kategori.
```c
void move_files() {
    struct Node *tmp = head;
    do
    {
        pid_t pid = fork();
        int status;
        if (pid == 0)
        {
            char *src = malloc(100);
            strcpy(src, HOME);
            strcat(src, "/shift2/drakor/");
            strcat(src, tmp->drakor->nama_file);
            char *dst = malloc(100);
            strcpy(dst, HOME);
            strcat(dst, "/shift2/drakor/");
            strcat(dst, tmp->drakor->kategori);
            strcat(dst, "/");
            strcat(dst, tmp->drakor->judul);
            strcat(dst, ".png");
            char *argv[] = {"cp", src, dst, NULL};
            execv("/usr/bin/cp", argv);
            exit(0);
        }
        wait(&status);
    } while (tmp = tmp->next);
}
```
E. Buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). 
```c
void create_txts()
{
    struct Node *tmp = head;
    do
    {
        char *file = malloc(100);
        strcpy(file, HOME);
        strcat(file, "/shift2/drakor/");
        strcat(file, tmp->drakor->kategori);
        strcat(file, "/data.txt");
        FILE *fptr;
        fptr = fopen(file, "w");
        if (fptr) {
            fprintf(fptr, "kategori : %s", tmp->drakor->kategori);
            fclose(fptr);
        }
    } while (tmp = tmp->next);
}

```


# SOAL 3
## CATATAN SOAL
-Tidak boleh memakai system().
-Tidak boleh memakai function C mkdir() ataupun rename().
-Gunakan exec dan fork
-Direktori “.” dan “..” tidak termasuk

## A. Membuat Folder modul2
Untuk  membuat folder di bahasa c digunakanlah execlp untuk menjalankan argumen sebagai command lalu digunakan -p agar jika parent folder belom dibuat maka parent folder akan dibuat juga. Lalu digunakan "while(wait(&status)) > 0" agar tidak exit dari function sebelum tugas dalam function selesai dikerjakan.
```
 void makeDirectory(char path[])
{
  if (fork() == 0)
    execlp("mkdir", "mkdir", "-p", path, NULL);
  while ((wait(&status)) > 0)
    ;
}
```

### 1. Membuat Folder darat
Gunakan function makeDirectory dengan memasukkan path untuk folder darat pada main.
```
char pathDarat[] = "/home/naufalariq/modul2/darat/";
makeDirectory(pathDarat);
```
### 2. Setelah 3s Membuat Folder air
Gunakan function makeDirectory dengan memasukkan path untuk folder air pada main.
```
char pathAir[] = "/home/naufalariq/modul2/air/";
sleep(3);
makeDirectory(pathAir);
```

## B. Melakukan Unzip File animal.zip
Untuk menjalankan command unzip di bahasa c digunakanlah execlp diisi dengan command yang ingin dijalankan yaitu unzip dan digunakan -d untuk membuat destinasi untuk hasil dari unzip dan digunakan -q ("quiet") agar tidak menghasilkan verbose pada terminal. Digunakkan while(wait(&status)) untuk dapat menghindari exit function sebelum script selesai dijalankan. 
```
void unzip(char destination[], char target[])
{
  if (fork() == 0)
    execlp("unzip", "unzip", "-d", destination, "-q", target, NULL);

  while ((wait(&status)) > 0)
    ;
}
```

Gunakan function unzip dengan mengisi path untuk target dan destination pada main.
```
char path[] = "/home/naufalariq/modul2/";
char targetZIP[] = "animal.zip";
unzip(path, targetZIP);
```

## C. Memindahkan file dari animal.zip
Pertama buka folder yang diinginkan yaitu folder animal lalu melakukan iterasi selama tidak NULL atau selama cursor file masih ada. Lalu setiap iterasi file pada folder dilakukan pengcopyan nama file (dir->d_name) ke array global hewan serta menambahkan sebanyak satu pada variabel count untuk menghitung berapa banyak jumlah file yang ada pada folder yang dibuka. Jika sudah selesai melakukan iterasi pada folder, folder akan ditutup. Kemudian list hewan sesuai kategori yang ada yaitu "air", "darat", dan "burung" dengan melakukan iterasi sebanyak jumlah hewan yang ada. Jika nama hewan mengandung kata "burung" (dapat digunakan fungsi string strstr) maka nama hewan tersebut akan dicopy ke array burung dan variabel burungSize akan ditambah sebanyak 1 (banyaknya jumlah file yang mengandung kata burung). Lalu jika hewan juga mengandung kata "darat" maka nama hewan tersebut juga akan dicopy ke array hewanDarat dan variabel countDarat akan ditambah sebanyak 1 (banyaknya jumlah file mengandung kata darat) dan akan melakukan pemanggilan function moveAnimalDaratOrAir dengan parameter type adalah "darat". Namun jika nama hewan mengandung kata "air" maka nama hewan tersebut akan dicopy ke array hewanAir dan variabel countAir akan ditambah sebanyak 1 dan akan melakukan pemanggilan function moveAnimalDaratOrAir dengan parameter type adalah "air".
```
void listAnimal(char path[])
{
  DIR *d;
  struct dirent *dir;
  int count = 0, countDarat = 0;
  d = opendir(path);

  if (d)
  {
    while ((dir = readdir(d)) != NULL)
    {
      if ((strstr(dir->d_name, "..") == 0) && (strcmp(dir->d_name, "") != 0) && (strcmp(dir->d_name, ".") != 0) && (strstr(dir->d_name, "txt") == 0))
      {
        strcpy(hewan[count], dir->d_name);
        count++;
      }
    }
    closedir(d);
  }

  for (int i = 0; i < count; i++)
  {
    char copyPath[100];
    strcpy(copyPath, path);

    if (strstr(hewan[i], "bird"))
    {
      strcpy(burung[burungSize], hewan[i]);
      burungSize++;
    }
    if (strstr(hewan[i], "darat"))
    {
      strcpy(hewanDarat[countDarat], hewan[i]);
      moveAnimalDaratOrAir(copyPath, i, "darat");
      countDarat++;
    }
    else if (strstr(hewan[i], "air"))
    {
      strcpy(hewanAir[countAir], hewan[i]);
      moveAnimalDaratOrAir(copyPath, i, "air");
      countAir++;
    }
  }
}
```

Untuk memindahkan dapat digunakan dengan "execlp("mv", "mv", path, dest, NULL)" dimana path adalah path source dari file dan dest adalah path source setelah dipindahkan nantinya. Untuk menentukan pembagiannya sesuai kategori digunakanlah strcmp antara type dan "air" dimana jika menghasilkan 0 maka artinya type dan "air" sama dan merupakan kategori air maka melakukan pengcopyan path air ke variabel dest, begitupun sebaliknya. Digunakkan while(wait(&status)) untuk dapat menghindari exit function sebelum script selesai dijalankan. 
```
void moveAnimalDaratOrAir(char path[], int index, char *type)
{
  strcat(path, hewan[index]);
  char dest[100];
  if (strcmp(type, "air") == 0)
    strcpy(dest, "/home/naufalariq/modul2/air");
  else
    strcpy(dest, "/home/naufalariq/modul2/darat");
  if (fork() == 0)
  {
    execlp("mv", "mv", path, dest, NULL);
  }
  while ((wait(&status)) > 0)
    ;
}
```

Dipanggillah function listAnimal dengan path animal untuk memindahkan file ke folder kategorinya masing-masing dan melakukan penglistan nama file hewan ke global array yang telah disediakan sesuai dengan kategori air dan darat pada fungsi main.
```
char pathAnimal[] = "/home/naufalariq/modul2/animal/";
listAnimal(pathAnimal);
```

### 1. Menghapus file yang tidak mengandung baik "air" maupun "darat"
Untuk dapat menghapus file serta folder yang tidak dibutuhkan yaitu animal dan file yang tidak sesuai dengan format dapat dilakukan langsung dengan rm -r ("recursive") path. Lalu digunakan while wait untuk dapat menghindari exit function sebelum script selesai dijalankan.
```
void removeDirectory(char path[])
{
  int status;
  if (fork() == 0)
    execlp("rm", "rm", "-r", path, NULL);
  while ((wait(&status)) > 0)
    ;
}
```

Karena kita ingin menghapus file yang berada di dalam folder animal serta folder animal, maka kita bisa langsung saja menghapus satu direktori dengan menggunakan function removeDirectory dan memparse path untuk animal pada main. 
```
char pathAnimal[] = "/home/naufalariq/modul2/animal/";
removeDirectory(pathAnimal);
```

## D. Menghapus file yang mengandung "burung" dari folder darat
Untuk dapat menghapus file yang diinginkan dapat dilakukan langsung dengan rm --force path. Lalu digunakan while wait untuk dapat menghindari exit function sebelum script selesai dijalankan.
```
void removeFile(char path[])
{
  int status;
  if (fork() == 0)
    execlp("rm", "rm", "--force", path, NULL);
  while ((wait(&status)) > 0)
    ;
}
```

Melakukan concanate string path darat dengan nama file burung lalu melakukan penginterasian sebanyak jumlah file yang mengandung kata "burung" pada file darat.
```
void removeBirdFile()
{
  for (int i = 0; i < burungSize; i++)
  {
    char source[100] = "/home/naufalariq/modul2/darat/";
    strcat(source, burung[i]);
    removeFile(source);
  }
}
```

Memanggil function removeBirdFile yang telah dijelaskan diatas pada main
```
removeBirdFile();
```

## E. Melakukan list untuk nama file dari folder air ke list.txt
Untuk format nama list yang digunakkan adalah UID_UID Permission(rwx)_namafile.jpg. Untuk mendapat UID dapat dengan mudah bisa didapatkan dengan melakukan check stat pada folder air lalu digunakanlah getpwuid dari path yang diisi, lalu untuk mendapat nama dapat dengan mudah dilakukan pw->pw_name. Lalu untuk mendapat UID Permission didapatkan dengan membuat char array dan dicek satu persatu untuk read, write, dan execute dengan S_IRUSR(untuk r), S_IRUSR(untuk w), dan S_IRUSR(untuk x). Lalu untuk tahap terakhir dapat digunakan dengan I/O pada C. Digunakkan open file dengan mode "a" (append) dan untuk menulis per line digunakan fprintf dengan format yang telah diminta. Lalu setelah selesai menggunakan file, akan dilakukan penutupan file untuk mencegah memory leak
```
void addList(int index)
{
  char permission[5];
  struct stat fs;
  int r;
  char path[] = "/home/naufalariq/modul2/air/";
  r = stat(path, &fs);
  if (r == -1)
  {
    fprintf(stderr, "File error\n");
    exit(1);
  }

  struct passwd *pw = getpwuid(fs.st_uid);
  if (fs.st_mode & S_IRUSR)
    permission[0] = 'r';
  if (fs.st_mode & S_IRUSR)
    permission[1] = 'w';
  if (fs.st_mode & S_IRUSR)
    permission[2] = 'x';

  if (pw != 0)
  {
    FILE *wfile = fopen("/home/naufalariq/modul2/air/list.txt", "a");
    fprintf(wfile, "%s_%s_%s\n", pw->pw_name, permission, hewanAir[index]);
    fclose(wfile);
  }
}
```
Panggil function AddList sebanyak file yang berada dalam folder air pada main.
```
for (int i = 0; i < countAir; i++)
    addList(i);
```
