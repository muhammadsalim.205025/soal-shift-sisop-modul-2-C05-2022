#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <wait.h>

int status;

void makeDir(char source[])
{
    if(fork() == 0)
    {
        char *argv[] = {"mkdir", "-p", source, NULL};
        execv("/bin/mkdir", argv);
    }
    while ((wait(&status)) > 0);
}
void unzip(char target[], char dest[])
{
    if(fork() == 0)
    {
        char *argv[] = {"unzip", "-q", target, "-d", dest, NULL};
        execv("/bin/unzip", argv);
    }
    while ((wait(&status)) > 0);
}

int main() {
    makeDir("/home/naufalariq/modul2/darat");
    sleep(3);
    makeDir("/home/naufalariq/modul2/air");

    unzip("animal.zip", "/home/naufalariq/modul2/");
}